package us.ldts.ktldts0

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    //Estas dos variables las declaramos como propiedades de la actividad.
    private lateinit var tapbutton: Button
    private lateinit var contador: TextView

    //Esta función es como el constructor de la actividad MainActivity:
    // se ejecuta cuando se inicia - on Create.
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        //Esto establece la vista --La que está difinida en el diseño/layout activity_main.xml
        setContentView(R.layout.activity_main)

        //Dentro de activity_main.xml hay dos elementos que nos interesan: un botón y un texto.
        //Las 2 propiedades de la clase MainActivity se "conectan" aquí con esos 2 elementos del layout.
        tapbutton = findViewById(R.id.buttontap)
        contador = findViewById(R.id.contador)

        //Y esto es como en cualquier lenguaje: establecemos un listener para los eventos Click del Botón.
        //Aquí definimos que ocurre cuando se clickea el botón.
        tapbutton.setOnClickListener {
            var anterior = contador.text.toString();
            if (anterior.equals("No taps yet!")) anterior = "0"
            var valor = anterior.toInt()
            contador.text = (valor + 1).toString()
        }
    }

}